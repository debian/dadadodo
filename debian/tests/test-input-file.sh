#!/bin/sh

set -eu

TXTFILEIN="$AUTOPKGTEST_TMP/in.txt"
TXTFILEOUT="$AUTOPKGTEST_TMP/out.txt"

{
    echo "The rose is red"
    echo "The violet's blue"
} > $TXTFILEIN

dadadodo $TXTFILEIN -o $TXTFILEOUT -c 1

if [ ! -s "$TXTFILEOUT" ]; then
   echo "ERROR in generating the output file $TXTFILEOUT"
   exit 1
fi
